// server.api.module.js

var mongoose = require('mongoose');

var ModuleSchema = new mongoose.Schema({
	title: { type: String, required: true, trim: true },
	contents: [ {
		title: String,
		order: String,
		type: {type: String, enum: ['content', 'script']},
		data: String
	} ],
	isPrivate: {type: Boolean, default: false},
  creator: mongoose.Schema.Types.ObjectId,
  editor: mongoose.Schema.Types.ObjectId,
  updateDate: { type: Date },
  createdDate: { type: Date }
});
		// data: mongoose.Schema.Types.Mixed

module.exports = mongoose.model('Module', ModuleSchema);
