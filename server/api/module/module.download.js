'use strict';

var path = require('path');
//var Module = require('./module.model');
var fs = require('fs');
var EasyZip = require('easy-zip').EasyZip;

exports.conf = {};
exports.conf.rootFolder = __dirname + '/usersaves/';

function isTmp() {
  return (path.basename(exports.tmpPath) === 'tmp' ? true : false);
}

exports.tmpPath = function(userdir, res) {
  var tpath;
  if (!isTmp(userdir))
    tpath = createTmp(userdir, res); 
  return tpath;
}

function createTmp(userdir, res) {
	userdir = userdir || '';
	fs.mkdir(exports.conf.rootFolder + userdir +'/tmp', function(err, module) {
		if (err) return res.json(500, err);
		if (module) return res.json(module);
		res.send(204);
	});
}

exports.saveFile = function(content, userdir, res) {
	fs.writeFile(exports.tmpPath(userdir, res) + content.title +
		(content.type ==='content' ? '.js' : '.lua'), 
		content.data, 
		function(/*err*/) {
			
	});
}

exports.saveModule = function(module, userdir, res) {
	for (var i = module.contents.length - 1; i >= 0; i--) {
		exports.saveFile(module.contents[i], userdir, res);
	}
};

exports.createArchive = function(directory, zipFileName, callback) {
	var zip = new EasyZip();
	zip.zipFolder(directory, function() {
		zip.writeToFile(zipFileName);
		callback(fs.createReadStream(zipFileName));
	});
};
