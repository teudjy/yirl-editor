'use strict';

var Module = require('./module.model');
var download = require('./module.download');

function isValidId (req, res){
  req.checkParams('id', 'Invalid id').isObjectId();

  var errors = req.validationErrors();
  if (!errors) {
    return true;
  }
  res.json(400, errors)
  return false;
}

exports.index = function(req, res) {
	Module
		.find()
		.or([
			{
				$and: [
					{'isPrivate': true},
					{'creator': req.session.passport.user._id}
				]
			},
			{'isPrivate': false},
		])
		.exec(function(err, modules) {
			if (err) return res.json(500, err);
			return res.json(modules);
		});
};

exports.show = function(req, res) {
	if (!isValidId(req, res)) return;
	var id = req.param('id');
	Module.findById(id, function(err, module) {
		if (err) return res.json(500, err);
		if (module) return res.json(module);
		res.send(204);
	});
};

exports.create = function(req, res) {
	var module = new Module(req.body);
  module.creator = req.session.passport.user._id;
  module.createDate = Date.now();
	module.save(function(err) {
		if (err) return res.json(400, err);
		return res.json(module);
	});
};

exports.update = function(req, res) {
  if(!isValidId(req,res)) return;
  var id = req.param('id');
  if(req.body._id) { delete req.body._id; }
  req.body.editor = req.session.passport.user._id;
  req.body.updateDate = Date.now();
  Module.findByIdAndUpdate(id, { $set: req.body }, function(err, module) {
    if(err) return res.json(500, err);
    res.json(module);
  });
};

exports.delete = function(req, res) {
	if (!isValidId(req, res)) return;
	var id = req.param('id');
	Module.findByIdAndRemove(id, function(err, module) {
		if (err) return res.json(500, err);
		if (module) return res.json(module);
		res.send(204);
	});
};

exports.download = function(req, res) {
	download.createArchive('server/api/module', '.tmp/plop2.zip', function(stream) {
		stream.pipe(res);
	});
	// if (!isValidId(req, res)) return;
	// var id = req.param('id');
	// Module.findById(id, function(err, module) {
	// 	if (err) return res.json(500, err);
	// 	if (module) {
	// 		download.saveModule(module, null, res);
	// 	}
	// 	res.send(204);
	// });
}

var unZip = require('unzip');
var fs = require('fs');

var archives = {
	'zip': function(path) {
		return unZip.Extract({ path: path });
	}
}

exports.upload = function(req, res) {
	var index = 'module';
	if (typeof(archives[req.files[index].extension]) === 'function') {
		var path = 'output/' + req.files[index].name;
		path = path.substring(0, path.length - 4);
		fs.createReadStream(req.files[index].path)
		.pipe(archives[req.files[index].extension](path))
		.on('close', function() {
			fs.readdir(path, function(err, folderFiles) {
				if (err)
					return res.send(500);
				var moduleName = folderFiles[0];
				path += '/' + moduleName;
				fs.readFile(path + '/' + 'StartingPoint', function(err, startingPointData) {
					if (err)
						return res.send(500);
					var splits = startingPointData.toString().split('\n');
					if (splits[0] !== 'version:1') return res.send(500);
					var contentNbr = 0;
					var contents = [];
					var addContent = function(fileName, type, id) {
						return function(err, data) {
							contentNbr--;
							if (!err) {
								contents.push({
									title: fileName.split('.')[0],
									order: id,
									data: data.toString()
								});
								if (contentNbr === 0) {
									var m = new Module({
										title: moduleName,
										creator: req.session.passport.user._id,
										createDate: Date.now(),
										contents: contents
									});
									m.save(function(err) {
										if (err) return res.json(400, err);
										return res.json(m);
									});
								}
							}
						};
					};
					splits.forEach(function(line) {
						var arr = line.split(':');
						var type = arr[0];
						var value = arr[1];
						if (type === 'content' && value.split('.')[1].toLowerCase() === 'json') {
							contentNbr++;
							fs.readFile(path + '/content/' + value, addContent(value, type, contentNbr - 1));
						} else if (type === 'script') {
							contentNbr++;
							fs.readFile(path + '/script/' + value, addContent(value, type, contentNbr - 1));
						}
					});
				});
			});
		});
	} else {
		return res.send(500);
	}
}
