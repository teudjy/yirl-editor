'use strict';

var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
	displayName: String,
	username: String,
	password: String,
	profile: mongoose.Schema.Types.Mixed
});