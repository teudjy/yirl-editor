'use strict';

var User = require('./user.model');

function isValidId (req, res){
  req.checkParams('id', 'Invalid id').isObjectId();

  var errors = req.validationErrors();
  if(!errors){
    return true;
  }
  res.json(400, errors)
  return false;
}

exports.index = function(req, res) {
	User
		.find()
		.exec(function(err, modules) {
			if (err) return res.json(500, err);
			return res.json(modules);
		});
};

exports.show = function(req, res) {
	if (!isValidId(req, res)) return;
	var id = req.param('id');
	User.findById(id, function(err, module) {
		if (err) return res.json(500, err);
		if (module) return res.json(module);
		res.send(204);
	});
};

exports.showMe = function(req, res) {
	var id = req.session.passport.user._id;
	User.findById(id, function(err, module) {
		if (err) return res.json(500, err);
		if (module) return res.json(module);
		res.send(204);
	});
};

exports.create = function(req, res) {
	var module = new User(req.body);
	module.save(function(err) {
		if (err) return res.json(400, err);
		return res.json(module);
	});
};

exports.update = function(req, res) {
  if(!isValidId(req,res)) return;

  if(req.body._id) {
    delete req.body._id;
  }
  var id = req.param('id');
  User.findByIdAndUpdate(id, { $set: req.body }, function(err, module) {
    if(err) return res.json(500, err);
    res.json(module);
  });
};

exports.delete = function(req, res) {
	if (!isValidId(req, res)) return;
	var id = req.param('id');
	User.findByIdAndRemove(id, function(err, module) {
		if (err) return res.json(500, err);
		if (module) return res.json(module);
		res.send(204);
	});
};
