'use strict';

var GoogleStrategy = require('passport-google').Strategy;
var express = require('express');

exports.setStrategy = function(passport, User) {
passport.use(new GoogleStrategy({
  returnURL: 'http://confident-thunder-47-166035.euw1.nitrousbox.com/auth/google/return',
  realm: 'http://confident-thunder-47-166035.euw1.nitrousbox.com/'
},
function(identifier, profile, done) {
	profile.identifier = identifier;
	profile.provider = 'google';
	User.findOne({'username': profile.emails[0].value})
    .exec(function(err, user) {
      if (err) return done(err);
      else if (user) return done(null, user);
      else {
        var newUser = new User({
          username: profile.emails[0].value,
          password: '',
          profile: profile
        });
        newUser.save(function(err) {
          if (err) return done(err);
          return done(null, newUser);
        });
      }
  	});
  }));
};

var scope = [
];

exports.getRouter = function(passport) {
  var router = express.Router();
  router.get('/', passport.authenticate('google'));
	router.get('/return', passport.authenticate('google', { successRedirect: '/', failureRedirect: '/login', scope: scope }));
  return router;
}
