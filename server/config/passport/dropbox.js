'use strict';

var DropboxStrategy = require('passport-dropbox-oauth2').Strategy;
var express = require('express');

exports.setStrategy = function(passport, User) {
  passport.use(new DropboxStrategy({
    clientID: '70bxvqdtcjav24t',
    clientSecret: '97puoq846sclz48',
    callbackURL: 'http://localhost:9000/auth/dropbox/callback'
  }, function(accessToken, refreshToken, profile, done) {
    User.findOne({'username': profile.id})
      .exec(function(err, user) {
        if (err) return done(err);
        else if (user) {
          user.profile = profile;
          return done(null, user);
        }
        else {
          var newUser = new User({
            username: profile.id,
            password: '',
            profile: profile
          });
          newUser.save(function(err) {
            if (err) return done(err);
            return done(null, newUser);
          });
        }
    });
  }));
};


exports.getRouter = function(passport) {
  var router = express.Router();
  router.get('/', passport.authenticate('dropbox-oauth2'));
  router.get('/callback', passport.authenticate('dropbox-oauth2', { successRedirect: '/', failureRedirect: '/login' }));
  return router;
}
