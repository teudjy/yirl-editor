'use strict';

var FacebookStrategy = require('passport-facebook').Strategy;
var express = require('express');

exports.setStrategy = function(passport, User) {
  passport.use(new FacebookStrategy({
    clientID : 305000899692889,
    clientSecret: 'd1666120f48e45ad86e07bb282ac1f69',
    callbackURL: 'http://localhost:9000/auth/facebook/callback',
    // callbackURL: 'http://confident-thunder-47-166035.euw1.nitrousbox.com/auth/facebook/callback',
  }, function(accesstoken, refreshtoken, profile, done) {
    User.findOne({'username': profile.id})
      .exec(function(err, user) {
        if (err) return done(err);
        else if (user) {
          user.profile = profile;
          return done(null, user);
        }
        else {
          var newUser = new User({
            username: profile.id,
            password: '',
            profile: profile
          });
          newUser.save(function(err) {
            if (err) return done(err);
            return done(null, newUser);
          });
        }
    });
  }));
};

var scope = [
];

exports.getRouter = function(passport) {
  var router = express.Router();
  router.get('/', passport.authenticate('facebook'));
	router.get('/callback', passport.authenticate('facebook', { successRedirect: '/',
	                                      failureRedirect: '/login', scope: scope }));
  return router;
}
