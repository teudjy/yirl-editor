'use strict';

var passport = require('passport');
var facebook = require('./facebook');
var dropbox = require('./dropbox');
var google = require('./google');
var User = require('../../api/user/user.model.js');

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

facebook.setStrategy(passport, User);
dropbox.setStrategy(passport, User);

module.exports = function(app) {
	app.use(passport.initialize());
	app.use(passport.session());

  app.use('/auth/facebook', facebook.getRouter(passport));
  app.use('/auth/dropbox', dropbox.getRouter(passport));
  app.use('/auth/google', google.getRouter(passport));
  
  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
  })
}
