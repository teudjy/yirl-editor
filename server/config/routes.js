// routes.js
'use strict';

var express = require('express');
var session = require('express-session');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var expressValidator = require('express-validator');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var multer = require('multer');

module.exports = function(app) {
  app.use(morgan('dev'));
  app.use(bodyParser.urlencoded({ extended :false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(session({secret: 'yirl-editor'}));
  app.use(multer({
    dest: './uploads/',
    fieldSize: 512
  }));

  app.use(expressValidator({
    customValidators: {
      isObjectId: function(value) {
        return mongoose.Types.ObjectId.isValid(value);
      }
    }
  }));

  require('./passport')(app);

  var isAuthenticated = function(req, res, next) {
    if (req.session.passport.user)
      next();
    else
      res.redirect('/auth/facebook/');
  };

  app.use('/api', isAuthenticated);
  app.get('/loggedin', function(req, res) {
    res.send(req.session.passport.user ? req.session.passport.user : 0);
  });
  app.use(express.static(path.join(__dirname, '..', '..', '.tmp')));
  app.use(express.static(path.join(__dirname, '..', '..', 'client')));
  app.use(express.static(path.join(__dirname, '..', '..', 'public')));


	app.use('/api/modules', require('../api/module'));
	app.use('/api/users', require('../api/user'));
};
