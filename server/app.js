'use strict';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

mongoose.connect(config.mongo.url);

var app = express();

require('./config/routes')(app);

app.listen (process.env.PORT || 9000);

console.log('Server is running');

module.exports = app;