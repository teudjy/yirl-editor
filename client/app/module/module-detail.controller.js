/* @ngInject */
function RouteConfig($stateProvider){
	$stateProvider.state('modules.list.detail', {
		url: '/{moduleId}',
		templateUrl: 'app/module/module-detail.html',
		resolve: {
			module: /* @ngInject */ function ($stateParams, Module) {
				return Module.get({id: $stateParams.moduleId}).$promise;
			},
			editor: /* @ngInject */ function(User, module) {
		    if (module.editor) {
		      return User.get({id: module.editor}).$promise;
		    }
		    return 'undefined';
		  },
			creator: /* @ngInject */ function(User, module) {
				if (module.creator) {
					return User.get({id: module.creator}).$promise;
				}
				return 'undefined';
			}
		},
		controller: /* @ngInject */function ($scope, module, editor, creator) {
      $scope.editor = editor;
      $scope.creator = creator;
			$scope.module = module;
		},
	});
}
angular.module('app').config(RouteConfig);