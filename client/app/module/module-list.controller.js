/* @ngInject */
function RouteConfig($stateProvider){
	$stateProvider.state('modules.list', {
		url: '/list',
		templateUrl: 'app/module/module-list.html',
		resolve: {
			modules: /* @ngInject */function (Module) {
				return Module.query().$promise;
			}
		},
		controller: /* @ngInject */function($scope, $state, $modal, modules) {
			$scope.modules = modules;
			$scope.modules.forEach(function(m) {
				m.isOpen = m._id === $state.params.moduleId;
			});

			$scope.add = function() {
				var modalInstance = $modal.open({
					templateUrl: 'app/module/module-add.html',
					controller: /* @ngInject */function($scope, $modalInstance, Module) {
						$scope.module = new Module();
						$scope.module.title = 'New Module';
						$scope.save = function() {
							$scope.module.$save(function(data) {
								$scope.module = new Module(data);
								$modalInstance.close($scope.module);
							});
						};
					},
				});

				modalInstance.result.then(function (module) {
					$state.transitionTo('modules.list.detail.edit', {
						moduleId: module._id
					}, {
						reload: true,
						inherit: false,
						notity: true
					});
				});
			};

			$scope.load = function() {
				var modalInstance = $modal.open({
					templateUrl: 'app/module/module-load.html',
					controller: /* @ngInject */function($scope, $modalInstance, Module) {
						$scope.zip = '';
						$scope.save = function() {
							$scope.module.$save(function(data) {
								$scope.module = new Module(data);
								$modalInstance.close($scope.zip);
							});
						};
					},
				});

				modalInstance.result.then(function (module) {
					alert('plop');
				});
			};
		},
	});
}

angular.module('app').config(RouteConfig);