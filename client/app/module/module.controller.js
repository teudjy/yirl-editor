/* @ngInject */
function RouteConfig($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/modules/list');

	$stateProvider.state('modules', {
		url: '/modules',
		templateUrl: 'app/module/module.html',
	});
}

angular.module('app').config(RouteConfig);