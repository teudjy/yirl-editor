/* @ngInject */
function ModuleFactory($resource) {
  return $resource('/api/modules/:id',
  	{id : '@_id'},
  	{
  		'update': {method: 'PUT'}
  	});
}

angular.module('app').factory('Module', ModuleFactory);
