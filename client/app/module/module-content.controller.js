/* @ngInject */
function RouteConfig($stateProvider){
	$stateProvider.state('modules.list.detail.content', {
		url: '/content/:id',
		views: {
			'@modules.list': {
				templateUrl: 'app/module/module-content.html',
				controller: /* @ngInject */function($scope, $stateParams, module) {
					for (var i = 0; i < module.contents.length; i++) {
						if (module.contents[i]._id === $stateParams.id) {
							$scope.content = module.contents[i];
						}
					}
					$scope.type = $scope.content.type === 'content' ? 'json' : 'lua';
				}
			}
		}
	});
}

angular.module('app').config(RouteConfig);