/* @ngInject */
function RouteConfig($stateProvider) {
	$stateProvider.state('modules.list.add', {
		template: '',
		controller: /* @ngInject */function($scope, $state, $modal) {
			$scope.add = function() {
				var modalInstance = $modal.open({
					templateUrl: 'app/module/module-add.html',
					controller: /* @ngInject */function($scope, $modalInstance, Module) {
						$scope.module = new Module();
						$scope.module.title = 'New Module';
						$scope.save = function() {
							$scope.module.$save(function(data) {
								$scope.module = new Module(data);
								$modalInstance.close($scope.module);
							});
						};
					},
				});

				modalInstance.result.then(function (module) {
					$state.transitionTo('modules.list.detail.edit', {
						moduleId: module._id
					}, {
						reload: true,
						inherit: false,
						notity: true
					});
				});
			};
			$scope.add();
		}
	});
}

angular.module('app').config(RouteConfig);