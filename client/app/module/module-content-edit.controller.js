/* @ngInject */
function RouteConfig($stateProvider) {
	$stateProvider.state('modules.list.detail.edit.content', {
		url: '/content/:id',
		views: {
			'@modules.list': {
				templateUrl: 'app/module/module-content-edit.html',
				controller: /* @ngInject */function($scope, $state, $stateParams, module) {
					for (var i = 0; i < module.contents.length; i++) {
						if (module.contents[i]._id === $stateParams.id) {
							$scope.content = module.contents[i];
						}
					}
					$scope.type = $scope.content.type === 'content' ? 'json' : 'lua';
					$scope.aceLoaded = function(_editor) {
						_editor.commands.addCommand({
							name: 'save',
							bindKey: { win: 'Ctrl-S', mac: 'Command-Option-S' },
							exec: function() {
								$scope.update();
							}
						});
						_editor.setReadOnly(false);
					};

		      $scope.update = function() {
						module.$update(function() {
							$state.transitionTo($state.current, $stateParams, {
								reload: true,
								inherit: false,
								notify: true
							});
						});
					};
		      $scope.delete = function() {
		      	var selectIndex = function (arr, func) {
		      		for (var i = arr.length - 1; i >= 0; i--) {
		      			if (func(arr[i]) === true) {
		      				return i;
		      			}
		      		}
		      	};
		      	var id = selectIndex(module.contents, function(content) {
		      		return content._id === $scope.content._id;
		      	});
		      	module.contents.splice(id, 1);
						module.$update(function() {
							$state.go('modules.list.detail.edit', $stateParams, {
								reload: true,
								inherit: false,
								notify: true
							});
						});
					};
				}
			}
		}
	});
}

angular.module('app').config(RouteConfig);