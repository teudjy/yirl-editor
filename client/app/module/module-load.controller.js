/* @ngInject */
function RouteConfig($stateProvider) {
	$stateProvider.state('modules.list.load', {
		template: '',
		controller: /* @ngInject */function($scope, $state, $modal) {
			$scope.add = function() {
				var modalInstance = $modal.open({
					templateUrl: 'app/module/module-load.html',
					controller: /* @ngInject */function($scope, $modalInstance, $upload) {
						$scope.files = '';
						$scope.load = function() {
							console.log("plop: ", $scope.files);
							if ($scope.files) {
								$upload.upload({
									url: '/api/modules/upload',
									method: 'POST',
									file: $scope.files,
									fileFormDataName: 'module'
								}).success(function(data, status, headers, config) {
									console.log(data, status, headers, config);
									$modalInstance.close(data);
								})
								.error(function(data, status, headers, config) {
									console.log('error');
									console.log(data, status, headers, config);
								});
							}
						};
					},
				});

				modalInstance.result.then(function (module) {
					$state.transitionTo('modules.list.detail.edit', {
						moduleId: module._id
					}, {
						reload: true,
						inherit: false,
						notity: true
					});
				});
			};
			$scope.add();
		}
	});
}

angular.module('app').config(RouteConfig);