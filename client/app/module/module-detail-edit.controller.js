/* @ngInject */
function RouteConfig($stateProvider){
	$stateProvider.state('modules.list.detail.edit', {
		url: '/edit',
		views: {
			'@modules.list': {
				templateUrl: 'app/module/module-detail-edit.html',
				controller: /* @ngInject */function($scope, $modal, $state, $stateParams, module) {
					$scope.module = module;
					$scope.save = function() {
						module.$update(function() {
							$state.transitionTo($state.current, $stateParams, {
								reload: true,
								inherit: false,
								notify: true
							});
						});
					};
					$scope.isMine = module.creator === $scope.user._id;
          $scope.add = function() {
          	var modalInstance = $modal.open({
          		templateUrl : 'app/module/module-content-add.html',
          		controller: /* @ngInject */function($scope, $modalInstance) {
          			$scope.content = {
          				title: '',
          				type: '',
          				data: '',
          			};
          			$scope.add = function() {
          				$modalInstance.close($scope.content);
          			};
          			$scope.cancel = function() {
          				$modalInstance.dismiss('cancel');
          			};
          		},
          	});
          	modalInstance.result.then(function(content) {
	            module.contents.push(content);
							module.$update(function(m) {
								m.contents.forEach(function(c) {
									if (c.title === content.title) {
										$stateParams.id = c._id;
										$state.go('modules.list.detail.edit.content', $stateParams, {
											reload: true,
											inherit: true,
											notify: true
										});
									}
								});
							});
          	});
          };
					$scope.delete = function() {
						$scope.module.$delete(function() {
							$state.transitionTo($state.current, $stateParams, {
								reload: true,
								inherit: false,
								notify: true
							});
						});
					};
				}
			}
		}
	});
}

angular.module('app').config(RouteConfig);