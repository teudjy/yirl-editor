// client/app/app.js
(function() {
	angular.module('app', ['ngResource', 'ui.router', 'ui.ace', 'ui.bootstrap', 'angularFileUpload']);

	/* @ngInject */
	function Loggedin($q, $http, $rootScope){
		// Initialize a new promise
		var deferred = $q.defer();
		// Make an AJAX call to check if the user is logged in
		$http.get('/loggedin').success(function (user){
			// Authenticated
			if (user !== '0') {
				return deferred.resolve(user);
				// Not Authenticated
			}
			else {
				$rootScope.message = 'You need to log in.';
				return deferred.reject();
			}
		});
		return deferred.promise;
	}
	angular.module('app').factory('Loggedin', Loggedin);

	/* @ngInject */
	function Run($rootScope, Loggedin, $state) {
		Loggedin.then(function(user) {
			if (user) {
				$rootScope.user = user;
			}
		});
		$rootScope.state = $state;
		$rootScope.moduleId = $state.params.moduleId;
	}

	angular.module('app').run(Run);
})();
