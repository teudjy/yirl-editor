/* @ngInject */
function UserFactory($resource) {
  return $resource('/api/users/:id',
  	{id : '@_id'},
  	{
  		'update': {method: 'PUT'}
  	});
}

angular.module('app').factory('User', UserFactory);
