Yirl-Editor
============

Yirl Online Web Editor.

Kickoff
-------

1. Install [Node.js](http://nodejs.org/download/)
2. Install [Git](http://git-scm.com/downloads) 
3. (sudo) npm install -g grunt-cli
4. (sudo) npm install -g bower
5. git clone https://bitbucket.org/teudjy/yirl-editor.git
6. cd web-jobboard
7. npm install
8. bower install
9. grunt serve
